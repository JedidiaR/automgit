package org.startingselenium.MyFirstSeleniumTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactPage {

	public ContactPage(WebDriver driver) {
		PageFactory.initElements(driver,this);
	}
	
	@FindBy (id = "comp-l3shey5h")
	WebElement form;
	
	public WebElement getForm() {
		return form;
	}
	
}
