package org.startingselenium.MyFirstSeleniumTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

    public HomePage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }
    
    @FindBy (id = "DrpDwnMn03label")
    WebElement enterprise;
    
    public EnterprisePage clickEnterprisePage(/*WebDriverWait wait,*/ WebDriver driver) {
    	//wait.until(ExpectedConditions.elementToBeClickable(enterprise));
    	enterprise.click();
    	return new EnterprisePage(driver);
    }
    
}
