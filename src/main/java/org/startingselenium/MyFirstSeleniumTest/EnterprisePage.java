package org.startingselenium.MyFirstSeleniumTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class EnterprisePage {
	
	public EnterprisePage(WebDriver driver) {
		PageFactory.initElements(driver,this);
	}
	
    @FindBy (id = "anchor-label_dataItem-l3bijl4q")
    WebElement contact;
    
    public ContactPage clickContactPage(WebDriver driver) {
//    	wait.until(ExpectedConditions.elementToBeClickable(contact));
    	contact.click();
    	return new ContactPage(driver);
    }

}
