package org.startingselenium.MyFirstSeleniumTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class MaClasseDeTest {

	WebDriver driver;
	WebDriverWait wait;
	
	@Before
	public void setUp() {
		System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");
	}
	
	// @After
	public void teadDown() {
		driver.quit();
		
	}

	
	@Test
    public void test() {
    	
		WebDriver driver = new FirefoxDriver();
//		driver.manage().timeouts().implicitlyWait(25,TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 1);
		driver.get("https://www.ecoleql.fr");
		driver.manage().window().maximize();
    	HomePage home = new HomePage(driver);
    	EnterprisePage tab = home.clickEnterprisePage(driver);
    	ContactPage contact = tab.clickContactPage(driver);
    	assertNotNull("FORMULAIRE NOT EXIST", contact.getForm());
    	// REMPLISSAGE DE CHAMP OK
    	driver.findElement(By.id("input_comp-l3shey6v1")).sendKeys("ROBERTO");
    	
    }
    
    
    public void accessAppli() throws InterruptedException {
		
		WebDriver driver = new FirefoxDriver();
//		driver.manage().timeouts().implicitlyWait(25,TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 1);
		driver.get("https://www.ecoleql.fr");
		driver.manage().window().maximize();
		//System.out.println("Page title is : " + driver.getTitle());
		assertEquals("Formation professionnelle | Ecole de la Qualité Logicielle | Reconversion", driver.getTitle());
		driver.findElement(By.xpath("//p[contains(text(),'Espace entreprise')]")).click();
		driver.findElement(By.id("anchor-label_dataItem-l3bijl4q")).click(); // find "contacter l'enteprise" By Id
		
		WebElement form = driver.findElement(By.xpath("//form"));
		assertNotNull("FORMULAIRE NON TROUVE", form);
		
		// REMPLISSAGE CHAMP OK // NE PAS VALIDER LE FORMULAIRE
		driver.findElement(By.id("input_comp-l3shey6v1")).sendKeys("ROBERTO");
		
		//Le but de ce test est d'atteindre le formulaire de contact (sans le remplir !) et de s'assurer qu'il est bien présent//
		//Malheureusement je n'arrive pas à descendre jusqu'à ce formulaire ... HELP//
    }
}
